import { Routes, Route } from "react-router-dom"
import "./App.scss"
import Home from "./Components/Home/Home"
import Layout from "./Components/Layout/Layout"

function App() {
  return (
    <Routes>
      <Route path="/" element={<Layout />} />
      <Route index element={<Home />} />
    </Routes>
  )
}

export default App
